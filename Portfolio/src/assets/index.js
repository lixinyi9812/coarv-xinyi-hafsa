import logo from "./logo.svg";
import backend from "./backend.png";
import creator from "./creator.png";
import mobile from "./mobile.png";
import web from "./web.png";
import github from "./github.png";
import menu from "./menu.svg";
import close from "./close.svg";

import css from "./tech/css.png";
import docker from "./tech/docker.png";
import figma from "./tech/figma.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import redux from "./tech/redux.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";
import threejs from "./tech/threejs.svg";

import vr from "./tech/vr.jpg"
import ar from "./tech/ar.png"
import gamedev from "./tech/gamedev.png"
import ml from "./tech/ml.png"
import catia from "./tech/catia.jpg"
import csharp from "./tech/csharp.png"
import cpp from "./tech/cpp.png"
import python from "./tech/python.png"
import opengl from "./tech/opengl.png"
import shadergraph from "./tech/shadergraph.png"
import unity from "./tech/unity.png"
import blender from "./tech/blender.jpg"
import acadomia from "./company/acadomia.png"
import medar from "./medar.png"
import accoord from "./company/accoord.png"
import hostedhouse from "./hostedhouse.png"
import escapegame from "./escapegame.webp"
import racinggame from "./racinggame.png"
import dyvem from "./company/dyvem.webp"
import gerkens from "./company/gerkens.png"
export {
  vr,
  ar,
  gamedev,
  ml,
  catia,
  csharp,
  cpp,
  python,
  opengl,
  shadergraph,
  unity,
  blender,
  acadomia,
  medar,
  hostedhouse,
  escapegame,
  racinggame,

  logo,
  backend,
  creator,
  mobile,
  web,
  github,
  menu,
  close,
  css,
  docker,
  figma,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  redux,
  tailwind,
  typescript,
  threejs,
  accoord,
  dyvem,
  gerkens,
  
};
