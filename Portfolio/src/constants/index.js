import {
    vr,
    ar,
    gamedev,
    ml,
    catia,
    csharp,
    cpp,
    python,
    opengl,
    shadergraph,
    unity,
    blender,
    javascript,
    html,
    threejs,
    css,
    reactjs,
    tailwind,
    nodejs,
    mongodb,
    git,
    dyvem,
    acadomia,
    accoord,
    medar,
    hostedhouse,
    escapegame,
    racinggame,
    gerkens,
  } from "../assets";
  
  export const navLinks = [
    {
      id: "about",
      title: "About",
    },
    {
      id: "work",
      title: "Work",
    },
    {
      id: "contact",
      title: "Contact",
    },
  ];
  
  const services = [
    {
      title: "Virtual Reality",
      icon: vr,
    },
    {
      title: "Augmented Reality",
      icon: ar,
    },
    {
      title: "Game Developer",
      icon: gamedev,
    },
    {
      title: "Machine Learning",
      icon: ml,
    },
    
  ];
  
  const technologies = [
    {
      name: "Catia",
      icon: catia,
    },
    {
      name: "C#",
      icon: csharp,
    },
    {
      name: "C++",
      icon: cpp,
    },
    {
      name: "Python",
      icon: python,
    },
    {
      name: "OpenGL",
      icon: opengl,
    },
    {
      name: "ShaderGraph",
      icon: shadergraph,
    },
    {
      name: "Unity",
      icon: unity,
    },
    {
      name: "Blender",
      icon: blender,
    },
    {
      name: "HTML 5",
      icon: html,
    },
    {
      name: "CSS 3",
      icon: css,
    },
    {
      name: "JavaScript",
      icon: javascript,
    },
    {
      name: "React JS",
      icon: reactjs,
    },
    
    {
      name: "Tailwind CSS",
      icon: tailwind,
    },
    {
      name: "Node JS",
      icon: nodejs,
    },
    {
      name: "MongoDB",
      icon: mongodb,
    },
    {
      name: "Three JS",
      icon: threejs,
    },
    {
      name: "git",
      icon: git,
    },
    
  ];
  

  const experiences = [
    {
      title: "Internship",
      company_name: "Company Gerkens",
      icon: gerkens,
      iconBg: "#383E56",
      date: "July 2022 - August 2022",
      points: [
        "Edit quotations with Mediabat software.",
        "Archiffage and digitization of data."
      ],
    },
    {
      title: "Project Manager",
      company_name: "Dyvem Logistics",
      icon: dyvem,
      iconBg: "#E6DEDD",
      date: "Jan 2021 - Feb 2022",
      points: [
        "Project leader of a group of six engineering students within the student project enterprise.",
      ],
    },
    {
      title: "Tutor",
      company_name: "Acadomia",
      icon: acadomia,
      iconBg: "#383E56",
      date: "January 2022 - June 2023",
      points: [
        "Provide math, physics and chemistry courses for high school students.",
      ],
    },
    {
      title: "volunteer",
      company_name: "Accoord",
      icon: accoord,
      iconBg: "#E6DEDD",
      date: "March 2022 - June 2023",
      points: [
        "Assist children with homework preparation.",
        "Organizing socio-cultural activities.",
      ],
    },
  ];
  
  
  
  const projects = [
    {
      name: "MedAR",
      description:
        "Augmented Reality application .Manipulate the 3D models of organs for the surgical preparation",
        
      tags: [
        {
          name: "unity",
          color: "blue-text-gradient",
        },
        {
          name: "vuforia",
          color: "green-text-gradient",
        },
        {
          name: "mrtk",
          color: "pink-text-gradient",
        },
        {
          name: "hololens2",
          color: "pink-text-gradient",
        },
      ],
      image: medar,
      source_code_link: "https://github.com/HafsaOuaj",
    },
    {
      name: "Haunted House",
      description:
        "Virtual reality with quest2 and the passtrought,in the context of a Hackaton in Clarté.",
        
      tags: [
        {
          name: "unity",
          color: "blue-text-gradient",
        },
        {
          name: "quest2",
          color: "green-text-gradient",
        },
        {
          name: "blender",
          color: "green-text-gradient",
        },
        
      ],
      image: hostedhouse,
      source_code_link: "https://github.com/HafsaOuaj",
    },
    {
      name: "Escape Game",
      description:
        "Virtual reality game.The player is enclosed in a cinema .He can found different reels in that room to solve the enigme.Each reel teleport the user into a famous movie where he should solve the enigme ",
        
      tags: [
        {
          name: "unity",
          color: "blue-text-gradient",
        },
        {
          name: "quest2",
          color: "green-text-gradient",
        },
        {
          name: "blender",
          color: "green-text-gradient",
        },
        
      ],
      image: escapegame,
      source_code_link: "https://github.com/HafsaOuaj"
    },
    {
      name: "Racing Game",
      description:
        "A 3D game, where the player controls a car by means of mobile in a racing game",
        
      tags: [
        {
          name: "unity",
          color: "blue-text-gradient",
        },       
      ],
      image: racinggame,
      source_code_link: "https://github.com/HafsaOuaj",
    },
  ];
  
  export { services, technologies, experiences,  projects };