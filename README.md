# COARV-xinyi-hafsa
This is the project for COARV by Xinyi and Hafsa. There are three parts: Vuforia, WebGL and UnityShader.  
- threejs-tutorial-xinyi: it is a simple example following the threejs toturial.  
- Portfolio: using WebGL to create a 3D web profile.  
- shader: using ShaderGraph to create a beautiful green trail effect.  
- vuforia: using vuforia to create a application allowing user to play on a hand-painted paper piano.  
