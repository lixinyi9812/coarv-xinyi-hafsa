using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBonClick : MonoBehaviour
{
    public AudioSource audioSource;
    //public AudioSource audioSource_do;
    //public AudioSource audioSource_re;
    //public AudioSource audioSource_mi;
    //public AudioSource audioSource_fa;
    //public AudioSource audioSource_sol;

    public VirtualButtonBehaviour vb;
    //public VirtualButtonBehaviour vb1;
    //public VirtualButtonBehaviour vb2;
    //public VirtualButtonBehaviour vb3;
    //public VirtualButtonBehaviour vb4;
    //public VirtualButtonBehaviour vb5;

    //Dictionary<VirtualButtonBehaviour, AudioSource> myDictionary = new Dictionary<VirtualButtonBehaviour, AudioSource>();

    // Start is called before the first frame update
    void Start()
    {
        //myDictionary.Add(vb1, audioSource_do);
        //myDictionary.Add(vb2, audioSource_re);
        //myDictionary.Add(vb3, audioSource_mi);
        //myDictionary.Add(vb4, audioSource_fa);
        //myDictionary.Add(vb5, audioSource_sol);



        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
        for(int i = 0; i < vbs.Length; i++)
        {
            vbs[i].RegisterOnButtonPressed(OnButtonPressed);
            vbs[i].RegisterOnButtonReleased(OnButtonReleased);
        }
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb) 
    {
        //AudioSource audioSource = myDictionary[vb];
        audioSource.Play();
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
